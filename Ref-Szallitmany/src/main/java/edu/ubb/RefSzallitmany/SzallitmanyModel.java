package edu.ubb.RefSzallitmany;

import java.util.Date;

public class SzallitmanyModel {
    private int id;
    private String name;
    private Date date;

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }

    public String toString() {
        return "Id " + id + " Name " + name + "date" + date;
    }

    public SzallitmanyModel() {
        super();
    }

    public SzallitmanyModel(int id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }
}