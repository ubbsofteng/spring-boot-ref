package edu.ubb.RefSzallitmany;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class SzallitmanyDAO {

    private static List<SzallitmanyModel> szallitmanyok = new ArrayList<>();
    static {
        szallitmanyok.add(new SzallitmanyModel(1, "Csiki pityoka", new Date()));
        szallitmanyok.add(new SzallitmanyModel(2, "Csiki male", new Date()));
        szallitmanyok.add(new SzallitmanyModel(3, "Csiki sor", new Date()));
        szallitmanyok.add(new SzallitmanyModel(4, "Csiki medve", new Date()));
    }


    public List<SzallitmanyModel> getAll() {
        return szallitmanyok;
    }

    public SzallitmanyModel getModelById (int id) {
        for (SzallitmanyModel model :szallitmanyok) {
            if (model.getId() == id) return model;
        }
        return null;
    }

    public void createNewModel (SzallitmanyModel model) {
        szallitmanyok.add(model);
    }
}