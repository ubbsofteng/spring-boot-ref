package edu.ubb.RefSzallitmany;

import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
@RestController
public class SzallitmanyController {
    @Autowired
    private SzallitmanyDAO szDAO;

    @GetMapping(path="/szallitmany")
    public List<SzallitmanyModel> getAll() {
        return szDAO.getAll();
    }

    @GetMapping(path="/szallitmany/{id}")
    public SzallitmanyModel getModelById(@PathVariable int id) {
        return szDAO.getModelById(id);
    }

    @PostMapping(path="/szallitmany")
    public void create(@RequestBody SzallitmanyModel szallitmanyModel) {
        szDAO.createNewModel(szallitmanyModel);
    }

}