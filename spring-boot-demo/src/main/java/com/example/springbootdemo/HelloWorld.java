package com.example.springbootdemo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class HelloWorld {

    @GetMapping(path="/helloCsilla")
    public String helloCsilla() {
        return "Hello, Csilla! 2018 UBB";
    }

}