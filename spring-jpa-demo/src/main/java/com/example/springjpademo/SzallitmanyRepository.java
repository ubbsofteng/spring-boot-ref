package com.example.springjpademo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SzallitmanyRepository extends JpaRepository<Szallitmany, Integer> {

}