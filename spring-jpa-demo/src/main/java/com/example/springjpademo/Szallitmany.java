package com.example.springjpademo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import java.util.Date;

@Entity
//@Table(name="szallitmany")
public class Szallitmany {

    @Id
    @GeneratedValue
    private int id;
    private Date date;
    private String name;


    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }

    public String toString() {
        return "Id " + id + " Name " + name + "date" + date;
    }

    public Szallitmany() {
        super();
    }

    public Szallitmany(int id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }
}